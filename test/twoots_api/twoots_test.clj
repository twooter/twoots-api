(ns twoots-api.twoots-test
  (:require [twoots-api.twoots :as sut]
            [humane-are.core]
            [clojure.test :as t :refer [deftest testing are]]))

(humane-are.core/install!)

(deftest get-twoot-elements-test
  (are [twoot-text expected] (= expected (sut/get-twoot-elements twoot-text))
    "@arunvickram"
    {:twoot.element/user-mention
     [{:twoot.element/type :twoot.element/user-mention
       :twoot/username "arunvickram"}]}

    "#hashtag"
    {:twoot.element/hashtag-mention
     [{:twoot.element/type :twoot.element/hashtag-mention
       :twoot/hashtag "hashtag"}]}

    "@arunvickram #hashtag"
    {:twoot.element/user-mention
     [{:twoot.element/type :twoot.element/user-mention
       :twoot/username "arunvickram"}]
     :twoot.element/hashtag-mention
     [{:twoot.element/type :twoot.element/hashtag-mention
       :twoot/hashtag "hashtag"}]}

    "@arunvickram @hellothere"
    {:twoot.element/user-mention
     [{:twoot.element/type :twoot.element/user-mention
       :twoot/username "arunvickram"}
      {:twoot.element/type :twoot.element/user-mention
       :twoot/username "hellothere"}]}

    "#hashtag @arunvickram @hellothere"
    {:twoot.element/user-mention
     [{:twoot.element/type :twoot.element/user-mention
       :twoot/username "arunvickram"}
      {:twoot.element/type :twoot.element/user-mention
       :twoot/username "hellothere"}]
     :twoot.element/hashtag-mention
     [{:twoot.element/type :twoot.element/hashtag-mention
       :twoot/hashtag "hashtag"}]}))
