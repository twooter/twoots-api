(ns twoots-api.server-test
  (:require [clojure.test :refer :all]
            [twoots-api.server :as server]
            [io.pedestal.test :refer :all]
            [donut.system :as ds]
            [io.pedestal.http :as bootstrap]
            [twoots-api.service :as service]))

(use-fixtures :each (ds/system-fixture ::server/test))

(deftest test-config
  (is (some? @(get-in ds/*system* [::ds/instances :app :config]))))
