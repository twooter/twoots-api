(ns twoots-api.twoots
  (:require [xtdb.api :as xt]
            [clojure.string :as str]))

(defn save-user! [node user]
  (xt/submit-tx node [[::xt/put user]]))

(defn save-tweet! [node tweet]
  (xt/submit-tx node [[::xt/put tweet]]))

(defn send-notifs! [tweet]
  (ex-info "Not implemented"
           {:causes #{:unimplemented}}))

(def mention-pattern (re-pattern "@\\w+"))
(def hashtag-pattern (re-pattern "#\\w+"))
(def link-pattern (re-pattern "\\w+\\.\\w+"))

(defn parse-word [word]
  (cond
    (re-find mention-pattern word)
    {:twoot.element/type :twoot.element/user-mention
     :twoot/username (subs (re-find mention-pattern word) 1)}

    (re-find hashtag-pattern word)
    {:twoot.element/type :twoot.element/hashtag-mention
     :twoot/hashtag (subs (re-find hashtag-pattern word) 1)}

    (re-find link-pattern word)
    {:twoot.element/type :twoot.element/link
     :twoot/link (re-find link-pattern word)}

    :else
    nil))

(defn get-twoot-elements [twoot-text]
  (let [words (str/split twoot-text #"\s+")
        elements (map parse-word words)]
    (group-by :twoot.element/type elements)))

(defn latest-tweets [node user]
  (xt/q
   (xt/db node)
   '{:find [?created-at (pull ?tweet [*])]
     :in [?user]
     :where [[?user :user/follows ?user-or-hashtag]
             (or [?tweet :twoot/user ?user-or-hashtag]
                 [?tweet :twoot.entities/hashtags ?user-or-hashtag])
             [?tweet :twoot/created-at ?created-at]]
     :order-by [[?created-at :desc]]}
   user))

(defn twoots-by-user [node user]
  (xt/q
   (xt/db node)
   '{:find [?created-at (pull ?tweet [*])]
     :in [?user]
     :where [[?tweet :twoot/user ?user]
             [?tweet :twoot/created-at ?created-at]]
     :order-by [[?created-at :desc]]}
   user))

(comment
  (def node (xt/start-node {}))

  (def user1 {:xt/id (java.util.UUID/randomUUID)
              :user/name "Twooter Dev"
              :user/screen-name "TwooterDev"
              :user/location "Internet"
              :user/url "https://dev.twootter.com/"
              :user/description "Your official source for Twooter Plaform news."
              :user/follows #{}})

  (def user2 {:xt/id (java.util.UUID/randomUUID)
              :user/name "Twooter Dev 2"
              :user/screen-name "TwooterDev2"
              :user/location "Internet"
              :user/url "https://dev.twootter.com/"
              :user/description "Your official source for Twooter Plaform news."
              :user/follows #{(:xt/id user1)}})

  (def user3 {:xt/id (java.util.UUID/randomUUID)
              :user/name "Twooter Dev 3"
              :user/screen-name "TwooterDev3"
              :user/location "Internet"
              :user/url "https://dev.twootter.com/"
              :user/description "Your official source for Twooter Plaform news."
              :user/follows #{"#hello"}})

  (def tweet1 {:xt/id (java.util.UUID/randomUUID)
               :twoot/created-at (java.time.Instant/now),
               :twoot/text
               "1/ Today we’re sharing our vision for the future of the Twitter API platform!\nhttps://t.co/XweGngmxlP",
               :twoot/user (:xt/id user1)
               :twoot/place {}
               :twoot.entities/hashtags []
               :twoot.entities/urls []
               :twoot.entities/user-mentions []})

  (def tweet2 {:xt/id (java.util.UUID/randomUUID)
               :twoot/created-at (java.time.Instant/now),
               :twoot/text
               "2/ hello",
               :twoot/user (:xt/id user1)
               :twoot/place {}
               :twoot.entities/hashtags []
               :twoot.entities/urls []
               :twoot.entities/user-mentions []})

  (def tweet3 {:xt/id (java.util.UUID/randomUUID)
               :twoot/created-at (java.time.Instant/now),
               :twoot/text
               "#hello there",
               :twoot/user (:xt/id user1)
               :twoot/place {}
               :twoot.entities/hashtags ["#hello"]
               :twoot.entities/urls []
               :twoot.entities/user-mentions []})

  user1

  (save-user! node user1)
  (save-user! node user3)

  (save-tweet! node tweet3)

  (twoots-by-user node (:xt/id user1))

  (latest-tweets node (:xt/id user3))

  {:twoot/created-at "Thu Apr 06 15:24:15 +0000 2017",
   :twoot/id-str "850006245121695744",
   :twoot/text
   "1/ Today we’re sharing our vision for the future of the Twitter API platform!\nhttps://t.co/XweGngmxlP",
   :twoot/user
   {:user/id 2244994945,
    :user/name "Twitter Dev",
    :user/screen_name "TwitterDev",
    :user/location "Internet",
    :user/url "https://dev.twitter.com/",
    :user/description
    "Your official source for Twitter Platform news, updates & events. Need technical help? Visit https://twittercommunity.com/ ⌨️ #TapIntoTwitter"},
   :twoot/place {},
   :twoot.entities/hashtags []
   :twoot.entities/urls []
   :twoot.entities/user-mentions []
   #_#_:twoot/entities
   {:hashtags [],
    :urls
    [{:url "https://t.co/XweGngmxlP",
      :unwound
      {:url "https://cards.twitter.com/cards/18ce53wgo4h/3xo1c",
       :title "Building the Future of the Twitter API Platform"}}],
    :user_mentions []}}

  {:createdAt ""
   :idStr ""
   :text ""
   :user
   {:id ""
    :name ""
    :screenName ""
    :location ""
    :url ""
    :description ""}
   :place {}
   :entities
   {:hashtags []
    :urls []
    :userMentions []}}

  )
