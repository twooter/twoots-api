(ns twoots-api.handlers
  (:require [twoots-api.twoots :as twoots]))

(def default-response
  {:status 200
   :body "Hello there"})

(defn get-all-twoots
  [req]
  default-response)

(defn save-twoot!
  [req]
  (let [db (get-in req [:db])
        twoot (twoots/save-tweet! db (:body req))])
  default-response)

(defn get-twoot
  [req]
  default-response)

(defn patch-twoot!
  [req]
  default-response)
