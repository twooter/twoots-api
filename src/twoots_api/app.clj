(ns twoots-api.app
  (:require [twoots-api.system :as system]
            [donut.system :as ds]))

(defn -main []
  (ds/signal system/system ::start))
