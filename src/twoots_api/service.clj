(ns twoots-api.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.interceptor.helpers :as interceptor]
            [com.github.sikt-no.clj-jwt :as clj-jwt]
            [ring.util.response :as ring-resp]))

(def jwks-endpoint "https://twooter-test.us.auth0.com/.well-known/jwks.json")

(defn unauthorized [text]
  {:status 401
   :headers {}
   :body text})

(defn decode-jwt [{:keys [required? jwk-endpoint]}]
  {:enter (fn [ctx]
            (if-let [auth-header (get-in ctx [:request :headers "authorization"])]
              (try (->> auth-header
                        (clj-jwt/unsign jwk-endpoint)
                        (assoc-in ctx [:request :claims]))

                   (catch Exception _
                     (assoc ctx :response (unauthorized "The token provided isn't valid"))))

              (if required?
                (assoc ctx :response (unauthorized "Token not provided"))
                (assoc-in ctx [:request :claims] {}))))})

;; TODO add POST /twoot/quote
;; TODO add PUT  /twoot/ - retwoots and likes

;; Consumed by twoots-api.server/create-server
;; See http/default-interceptors for additional options you can configure
(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; ::http/interceptors []
              ::http/routes []

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::http/allowed-origins ["scheme://host:port"]

              ;; Tune the Secure Headers
              ;; and specifically the Content Security Policy appropriate to your service/application
              ;; For more information, see: https://content-security-policy.com/
              ;;   See also: https://github.com/pedestal/pedestal/issues/499
              ;;::http/secure-headers {:content-security-policy-settings {:object-src "'none'"
              ;;                                                          :script-src "'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:"
              ;;                                                          :frame-ancestors "'none'"}}

              ;; Root for resource interceptor that is available by default.
              ::http/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ;;  This can also be your own chain provider/server-fn -- http://pedestal.io/reference/architecture-overview#_chain_provider
              ::http/type :jetty
              ;;::http/host "localhost"
              ::http/port 8080
              ;; Options to pass to the container (Jetty)
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false
                                        ;; Alternatively, You can specify your own Jetty HTTPConfiguration
                                        ;; via the `:io.pedestal.http.jetty/http-configuration` container option.
                                        ;:io.pedestal.http.jetty/http-configuration (org.eclipse.jetty.server.HttpConfiguration.)
                                        }})
