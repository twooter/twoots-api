(ns twoots-api.system
  (:require
   [aero.core :refer [read-config]]
   [clojure.java.io :as io]
   [donut.system :as ds]
   [io.pedestal.http :as http-server]
   [twoots-api.server :as app]))

(def Config
  #::ds{:start (fn [{{:keys [config-file]} ::ds/config}]
                 (read-config (io/resource config-file)))
        :config {:config-file "config.edn"}})

(def Server
  #::ds{:start (fn [{{:keys [config]} ::ds/config}]
                 (app/run-dev))
        :stop (fn [{::ds/keys [instance]}] (http-server/stop instance))
        :config {:config (ds/ref [:app :config])}})

(def system
  {::ds/defs
   {:app
    {:config Config
     :server Server}}})

(defmethod ds/named-system :donut.system/repl
  [_]
  system)

(defmethod ds/named-system ::test
  [_]
  system)
