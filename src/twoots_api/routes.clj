(ns twoots-api.routes
  (:require [reitit.http :as http]
            [reitit.pedestal :as pedestal]
            [reitit.ring :as ring]
            [reitit.coercion.malli]
            [muuntaja.core :as m]
            [twoots-api.handlers :as handlers]
            [reitit.http.interceptors.parameters :as parameters]
            [reitit.http.interceptors.muuntaja :as muuntaja]
            [reitit.http.interceptors.exception :as exception]
            [reitit.http.interceptors.multipart :as multipart]
            [reitit.http.coercion :as coercion]
            [reitit.dev.pretty :as pretty]))

(def router-opts
  {:exception pretty/exception
   :data {:coercion reitit.coercion.malli/coercion
          :muuntaja m/instance
          :interceptors [;; query-params & form-params
                         (parameters/parameters-interceptor)
                         ;; content-negotiation
                         (muuntaja/format-negotiate-interceptor)
                         ;; encoding response body
                         (muuntaja/format-response-interceptor)
                         ;; exception handling
                         (exception/exception-interceptor)
                         ;; decoding request body
                         (muuntaja/format-request-interceptor)
                         ;; coercing response bodys
                         (coercion/coerce-response-interceptor)
                         ;; coercing request parameters
                         (coercion/coerce-request-interceptor)
                         ;; multipart
                         (multipart/multipart-interceptor)]}})

(def routes
  [["/twoots"
    {:get {:summary "get all twoots"
           :handler handlers/get-all-twoots}
     :post {:summary "post a new twoot"
            :handler handlers/save-twoot!}}

    ["/:id"
     {:get {:summary "get twoot by id"
            :handler handlers/get-twoot}
      :patch {:summary "add likes and other modifications"
              :handler handlers/patch-twoot!}}]]])

(def router
  (pedestal/routing-interceptor
   (http/router routes router-opts)))

(comment
  (def app (ring/ring-handler (ring/router routes)))

  )
