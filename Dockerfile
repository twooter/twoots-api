FROM amazoncorretto:20-alpine-jdk
MAINTAINER Your Name <you@example.com>

ADD target/twoots-api-0.0.1-SNAPSHOT-standalone.jar /twoots-api/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/twoots-api/app.jar"]
